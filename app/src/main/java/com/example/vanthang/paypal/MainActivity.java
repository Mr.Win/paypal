package com.example.vanthang.paypal;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.braintreepayments.api.interfaces.HttpResponseCallback;
import com.braintreepayments.api.internal.HttpClient;
import com.braintreepayments.api.models.PaymentMethodNonce;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_CODE = 123;
    final String API_GET_TOKEN = "http://192.168.1.102/braintree/main.php";
    final String API_GET_CHECKOUT = "http://192.168.1.102/braintree/checkout.php";
    String token, amount;
    HashMap<String, String> paramHash;

    Button btnPay;
    EditText edtAmount;
    LinearLayout llHolder,llWaiting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnPay=findViewById(R.id.btnPay);
        edtAmount=findViewById(R.id.edtPrice);
        llHolder=findViewById(R.id.llHolder);
        llWaiting=findViewById(R.id.llwaiting);
        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBraintreeSubmit();
            }
        });
        new HttpRequest().execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode==REQUEST_CODE)
        {
            if (resultCode==RESULT_OK)
            {
                DropInResult dropInResult=data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                PaymentMethodNonce paymentMethodNonce=dropInResult.getPaymentMethodNonce();
                String stringNonce=paymentMethodNonce.getNonce();
                Log.d("mylog","Result"+stringNonce);
                // Send payment price with the nonce
                // use the result to update your UI and send the payment method nonce to your server
                if (!edtAmount.getText().toString().isEmpty())
                {
                    amount=edtAmount.getText().toString();
                    paramHash=new HashMap<>();
                    paramHash.put("amount",amount);
                    paramHash.put("nonce",stringNonce);
                    sendPaymentDetails();
                }
                else
                {
                    Toast.makeText(this, "Please enter valid amount", Toast.LENGTH_SHORT).show();
                }
            }else if (resultCode==RESULT_CANCELED)
            {
                Toast.makeText(this, "User Cancel", Toast.LENGTH_SHORT).show();
            }else {
                Exception error= (Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void sendPaymentDetails() {
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, API_GET_CHECKOUT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.toString().contains("Successful")) {
                    Toast.makeText(MainActivity.this, "Transaction successful", Toast.LENGTH_LONG).show();
                } else
                    Toast.makeText(MainActivity.this, "Transaction failed", Toast.LENGTH_LONG).show();
                Log.d("mylog", "Final Response: " + response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("mylog", "Volley error : " + error.toString());

            }
        }) {
            @Override
            protected Map<String, String> getParams(){
                if (paramHash == null)
                    return null;
                Map<String, String> params = new HashMap<String, String>();
                for (String key : paramHash.keySet())
                {
                    params.put(key, paramHash.get(key));
                    Log.d("mylog", "Key : " + key + " Value : " + paramHash.get(key));

                }
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }
    private void onBraintreeSubmit() {
        DropInRequest dropInRequest=new DropInRequest().clientToken(token);
        startActivityForResult(dropInRequest.getIntent(this),REQUEST_CODE);
    }

    private class HttpRequest extends AsyncTask{
        ProgressDialog progress;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = new ProgressDialog(MainActivity.this, android.R.style.Theme_DeviceDefault_Dialog);
            progress.setCancelable(false);
            progress.setMessage("We are contacting our servers for token, Please wait");
            progress.setTitle("Getting token");
            progress.show();
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            HttpClient client = new HttpClient();
            client.get(API_GET_TOKEN, new HttpResponseCallback() {
                @Override
                public void success(final String responseBody) {
                    Log.d("mylog", responseBody);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //Hide group waiting
                            llWaiting.setVisibility(View.GONE);
                            //show group payment
                            Toast.makeText(MainActivity.this, "Successfully got token", Toast.LENGTH_SHORT).show();
                            llHolder.setVisibility(View.VISIBLE);
                            //set token
                            token = responseBody;
                        }
                    });

                }

                @Override
                public void failure(Exception exception) {
                    final Exception ex = exception;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.this, "Failed to get token: " + ex.toString(), Toast.LENGTH_LONG).show();
                        }
                    });
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            progress.dismiss();
        }
    }
}
